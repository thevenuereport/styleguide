module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: 'babel-eslint'
  },
  extends: ['plugin:vue/recommended', 'eslint:recommended', 'prettier/vue'],
  plugins: ['prettier'],
  rules: {
    'vue/component-name-in-template-casing': ['error', 'PascalCase']
  }
};
