export default {
  SET_BG_COLOR(state, color) {
    state.bgColor = color;
  },
  SET_EMAIL_FIELD(state, email) {
    state.email = email;
  },
  SET_NAME_FIELD(state, name) {
    state.name = name;
  },
  SET_PASSWORD_FIELD(state, password) {
    state.password = password;
  },
  SET_PHONE_FIELD(state, phone) {
    state.phone = phone;
  },
  SET_SEARCH(state, query) {
    state.search = query;
  },
  SET_SEARCH_RESULT(state, result) {
    state.result = result;
  }
};
