import venues from '../objectRefs/venues';

export default {
  bgColor: '#fff',
  email: '',
  list: [
    'Apple',
    'Banana',
    'Orange',
    'Mango',
    'Pear',
    'Peach',
    'Grape',
    'Tangerine',
    'Pineapple',
    'San Diego',
    'Lake Tahoe',
    'Las Vegas',
    'New York'
  ],
  name: '',
  password: '',
  phone: '',
  search: '',
  result: '',
  venues: [...venues]
};
