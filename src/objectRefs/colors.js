export default [
  {
    name: 'primary',
    color: '#B59A76'
  },
  {
    name: 'secondary',
    color: '#62727B'
  },
  {
    name: 'success',
    color: '#439889'
  },
  {
    name: 'danger',
    color: '#FF606A'
  },
  {
    name: 'warning',
    color: '#F9D982'
  },
  {
    name: 'info',
    color: '#00838F'
  },
  {
    name: 'light',
    color: '#8C8C8C'
  },
  {
    name: 'dark',
    color: '#37474F'
  },
  {
    name: 'lighter',
    color: '#D9D9D9'
  },
  {
    name: 'lightest',
    color: '#F8F8F8'
  },
  {
    name: 'white',
    color: '#FFFFFF'
  }
];
