import { faArrowRight } from '@fortawesome/free-solid-svg-icons';
export default [
  {
    name: 'primary',
    iconName: ''
  },
  {
    name: 'secondary',
    iconName: ''
  },
  {
    name: 'outline',
    iconName: ''
  },
  {
    name: 'outline-arrow',
    iconName: faArrowRight
  },
  {
    name: 'outline-pill',
    iconName: ''
  },
  {
    name: 'outline-pill-arrow',
    iconName: faArrowRight
  },
  {
    name: 'outline-pill-reversed',
    iconName: ''
  },
  {
    name: 'outline-pill-reversed-arrow',
    iconName: faArrowRight
  }
];
