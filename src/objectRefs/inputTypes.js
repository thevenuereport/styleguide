import { faPhone, faKey, faEnvelope } from '@fortawesome/free-solid-svg-icons';

export default [
  {
    name: 'email',
    iconName: faEnvelope
  },
  {
    name: 'password',
    iconName: faKey
  },
  {
    name: 'phone',
    iconName: faPhone
  }
];
