"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  name: 'click-outside',
  abstract: true,
  props: {
    handler: {
      type: Function,
      required: true
    }
  },
  mounted: function mounted() {
    document.addEventListener('click', this.handleClickOutside, true);
  },
  beforeDestroy: function beforeDestroy() {
    document.removeEventListener('click', this.handleClickOutside, true);
  },
  methods: {
    handleClickOutside: function handleClickOutside(e) {
      if (!this.$el.contains(e.target)) {
        this.handler(e);
      }
    }
  },
  render: function render() {
    return this.$slots.default && this.$slots.default[0];
  }
};
exports.default = _default;
"use strict";

/**
 * @mixin
 */
module.exports = {
  props: {
    /**
     * Set size of the element
     */
    size: {
      type: String,
      default: "14px"
    }
  }
};
"use strict";

require("core-js/modules/es6.regexp.split");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  name: 'click-outside',
  abstract: true,
  props: {
    handler: {
      type: Function,
      required: true
    }
  },
  mounted: function mounted() {
    document.addEventListener('click', this.handleClickOutside, true);
  },
  beforeDestroy: function beforeDestroy() {
    document.removeEventListener('click', this.handleClickOutside, true);
  },
  methods: {
    handleClickOutside: function handleClickOutside(e) {
      if (!this.$el.contains(e.target)) {
        this.handler(e);
      }
    }
  },
  render: function render() {
    return this.$slots.default && this.$slots.default[0];
  }
};
exports.default = _default;
"use strict";
/**
 * @mixin
 */


module.exports = {
  props: {
    /**
     * Set size of the element
     */
    size: {
      type: String,
      default: "14px"
    }
  }
};
"use strict";

var _interopRequireWildcard = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard");

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

var _vue = _interopRequireDefault(require("vue"));

var _App = _interopRequireDefault(require("./App.vue"));

var _router = _interopRequireDefault(require("./router"));

var _index = _interopRequireDefault(require("./store/index"));

require("./registerServiceWorker");

var _fontawesomeSvgCore = require("@fortawesome/fontawesome-svg-core");

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _vueFontawesome = require("@fortawesome/vue-fontawesome");

var _veeValidate = require("vee-validate");

var rules = _interopRequireWildcard(require("vee-validate/dist/rules"));

require("./utils/filters/index");

for (var rule in rules) {
  (0, _veeValidate.extend)(rule, rules[rule]);
}

_vue.default.component('ValidationProvider', _veeValidate.ValidationProvider);

_fontawesomeSvgCore.library.add(_freeSolidSvgIcons.faUserSecret, _freeSolidSvgIcons.faArrowRight, _freeSolidSvgIcons.faEnvelope, _freeSolidSvgIcons.faAddressBook, _freeSolidSvgIcons.faPhone, _freeSolidSvgIcons.faKey);

_vue.default.component('font-awesome-icon', _vueFontawesome.FontAwesomeIcon);

_vue.default.config.productionTip = false;
new _vue.default({
  router: _router.default,
  store: _index.default,
  render: function render(h) {
    return h(_App.default);
  }
}).$mount('#app');
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _default = [{
  name: 'primary',
  iconName: ''
}, {
  name: 'secondary',
  iconName: ''
}, {
  name: 'outline',
  iconName: ''
}, {
  name: 'outline-arrow',
  iconName: _freeSolidSvgIcons.faArrowRight
}, {
  name: 'outline-pill',
  iconName: ''
}, {
  name: 'outline-pill-arrow',
  iconName: _freeSolidSvgIcons.faArrowRight
}, {
  name: 'outline-pill-reversed',
  iconName: ''
}, {
  name: 'outline-pill-reversed-arrow',
  iconName: _freeSolidSvgIcons.faArrowRight
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  name: 'primary',
  color: '#B59A76'
}, {
  name: 'secondary',
  color: '#62727B'
}, {
  name: 'success',
  color: '#439889'
}, {
  name: 'danger',
  color: '#FF606A'
}, {
  name: 'warning',
  color: '#F9D982'
}, {
  name: 'info',
  color: '#00838F'
}, {
  name: 'light',
  color: '#8C8C8C'
}, {
  name: 'dark',
  color: '#37474F'
}, {
  name: 'lighter',
  color: '#D9D9D9'
}, {
  name: 'lightest',
  color: '#F8F8F8'
}, {
  name: 'white',
  color: '#FFFFFF'
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _default = [{
  name: 'email',
  iconName: _freeSolidSvgIcons.faEnvelope
}, {
  name: 'password',
  iconName: _freeSolidSvgIcons.faKey
}, {
  name: 'phone',
  iconName: _freeSolidSvgIcons.faPhone
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  img: '',
  title: 'The Westerly',
  description: 'This is the westerly',
  location: 'Nowhere',
  price: 0,
  capacity: 5,
  fav: false
}, {
  img: '',
  title: 'The Grand',
  description: 'This is the Grand',
  location: 'Everywhere',
  price: 0,
  capacity: 10,
  fav: false
}, {
  img: '',
  title: 'The Lincoln',
  description: 'The Lincoln, oh yes.',
  location: 'There',
  price: 0,
  capacity: 300,
  fav: false
}, {
  img: '',
  title: 'The Best',
  description: 'The best of the best',
  location: 'Here',
  price: 0,
  capacity: 20,
  fav: false
}, {
  img: '',
  title: 'The Westerly',
  description: 'This is the westerly',
  location: 'Nowhere',
  price: 0,
  capacity: 5,
  fav: false
}, {
  img: '',
  title: 'The Grand',
  description: 'This is the Grand',
  location: 'Everywhere',
  price: 0,
  capacity: 10,
  fav: false
}];
exports.default = _default;
"use strict";

var _registerServiceWorker = require("register-service-worker");
/* eslint-disable no-console */


if (process.env.NODE_ENV === "production") {
  (0, _registerServiceWorker.register)("".concat(process.env.BASE_URL, "service-worker.js"), {
    ready: function ready() {
      console.log("App is being served from cache by a service worker.\n" + "For more details, visit https://goo.gl/AFskqB");
    },
    registered: function registered() {
      console.log("Service worker has been registered.");
    },
    cached: function cached() {
      console.log("Content has been cached for offline use.");
    },
    updatefound: function updatefound() {
      console.log("New content is downloading.");
    },
    updated: function updated() {
      console.log("New content is available; please refresh.");
    },
    offline: function offline() {
      console.log("No internet connection found. App is running in offline mode.");
    },
    error: function error(_error) {
      console.error("Error during service worker registration:", _error);
    }
  });
}

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/esm/interopRequireWildcard"));

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router")); // import Home from "./views/Home.vue";


_vue.default.use(_vueRouter.default);

var _default = new _vueRouter.default({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/buttons',
    name: 'buttons',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Buttons.vue"));
      });
    }
  }, {
    path: '/colors',
    name: 'colors',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Colors.vue"));
      });
    }
  }, {
    path: '/layout',
    name: 'layout',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Layout.vue"));
      });
    }
  }, {
    path: '*',
    redirect: '/buttons',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Buttons.vue"));
      });
    }
  }]
});

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {};
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {};
exports.default = _default;
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vuex = _interopRequireDefault(require("vuex"));

var _state = _interopRequireDefault(require("./state"));

var _actions = _interopRequireDefault(require("./actions"));

var _getters = _interopRequireDefault(require("./getters"));

var _modules = _interopRequireDefault(require("./modules"));

var _mutations = _interopRequireDefault(require("./mutations"));

_vue.default.use(_vuex.default);

var _default = new _vuex.default.Store({
  state: _state.default,
  getters: _getters.default,
  mutations: _mutations.default,
  actions: _actions.default,
  modules: _modules.default
});

exports.default = _default;
"use strict";

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  SET_BG_COLOR: function SET_BG_COLOR(state, color) {
    state.bgColor = color;
  },
  SET_EMAIL_FIELD: function SET_EMAIL_FIELD(state, email) {
    state.email = email;
  },
  SET_NAME_FIELD: function SET_NAME_FIELD(state, name) {
    state.name = name;
  },
  SET_PASSWORD_FIELD: function SET_PASSWORD_FIELD(state, password) {
    state.password = password;
  },
  SET_PHONE_FIELD: function SET_PHONE_FIELD(state, phone) {
    state.phone = phone;
  },
  SET_SEARCH: function SET_SEARCH(state, query) {
    state.search = query;
  },
  SET_SEARCH_RESULT: function SET_SEARCH_RESULT(state, result) {
    state.result = result;
  }
};
exports.default = _default;
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray"));

var _venues = _interopRequireDefault(require("../objectRefs/venues"));

var _default = {
  bgColor: '#fff',
  email: '',
  list: ['Apple', 'Banana', 'Orange', 'Mango', 'Pear', 'Peach', 'Grape', 'Tangerine', 'Pineapple', 'San Diego', 'Lake Tahoe', 'Las Vegas', 'New York'],
  name: '',
  password: '',
  phone: '',
  search: '',
  result: '',
  venues: (0, _toConsumableArray2.default)(_venues.default)
};
exports.default = _default;
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addComma = void 0;

var _vue = _interopRequireDefault(require("vue"));
/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */


var addComma = function addComma(value) {
  return value + ', ';
};

exports.addComma = addComma;

_vue.default.filter('addComma', addComma);

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.capitalize = void 0;

var _vue = _interopRequireDefault(require("vue"));

var capitalize = function capitalize(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
};

exports.capitalize = capitalize;

_vue.default.filter('capitalize', capitalize);

"use strict";

require("./addComma.filter");

require("./capitalize.filter");

require("./lastFour.filter");

require("./lowercase.filter");

require("./percentage.filter");

require("./truncate.filter");

require("./truncateEmail.filter");

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lastFour = void 0;

var _vue = _interopRequireDefault(require("vue"));
/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */


var lastFour = function lastFour(value) {
  return value.slice(value.length - 4);
};

exports.lastFour = lastFour;

_vue.default.filter('lastFour', lastFour);

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lowercase = void 0;

var _vue = _interopRequireDefault(require("vue"));
/**
 *  Filter that accepts a string and makes lowercase all capital letters
 *  @return lowercase
 */


var lowercase = function lowercase(value) {
  return value.toLowerCase();
};

exports.lowercase = lowercase;

_vue.default.filter('lowercase', lowercase);

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.numeralFilter = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _numeral = _interopRequireDefault(require("numeral"));
/**
 *  Filter that accepts a number and format pattern
 *
 *  common patterns:
 *      currency: $0,0.00'
 *      thousands: '0,0'
 *      percent: '0%'
 *
 *  See http://numeraljs.com/ for a complete list of patterns
 *
 *  @return number
 */


var numeralFilter = function numeralFilter(value, pattern) {
  return value && (0, _numeral.default)(value).format(pattern);
};

exports.numeralFilter = numeralFilter;

_vue.default.filter('numeral', numeralFilter);

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.percentageFilter = void 0;

var _vue = _interopRequireDefault(require("vue"));

var percentageFilter = function percentageFilter() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var decimals = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return "".concat((value * 100).toFixed(decimals), "%");
};

exports.percentageFilter = percentageFilter;

_vue.default.filter('percentage', percentageFilter);

"use strict";

var _percentage = require("./percentage.filter");

describe('percentage.filter', function () {
  var number = 0.842922;
  it('formats number', function () {
    expect((0, _percentage.percentageFilter)(number)).toBe('84%');
  });
  it('formats number when given decimal places', function () {
    expect((0, _percentage.percentageFilter)(number, 2)).toBe('84.29%');
  });
  it('formats null correctly', function () {
    expect((0, _percentage.percentageFilter)(null)).toBe('0%');
  });
  it('formats null correctly when given decimal places', function () {
    expect((0, _percentage.percentageFilter)(null, 2)).toBe('0.00%');
  });
});
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timeFilter = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));
/**
 *  Filter that accepts an ISO date and format pattern
 *
 *  See https://momentjs.com/timezone/ for patterns
 *
 *  @return string
 */


var timeFilter = function timeFilter(value, format) {
  if (!value || value.hasOwnProperty('date') && !value.date) {
    return 'Invalid date';
  }

  return (0, _momentTimezone.default)(value.date || value).format(format);
};

exports.timeFilter = timeFilter;

_vue.default.filter('time', timeFilter);

"use strict";

var _time = require("./time.filter");

describe('time.filter', function () {
  var unformattedDate = '2018-07-18 18:22:59';
  var formattedDate = 'Wed, Jul 18, 2018 6:22 PM';
  it('returns formatted string', function () {
    expect((0, _time.timeFilter)(unformattedDate, 'llll')).toBe(formattedDate);
  });
  it('returns formatted string when passed an object (value.date)', function () {
    var created = {
      date: unformattedDate
    };
    expect((0, _time.timeFilter)(created.date, 'llll')).toBe(formattedDate);
  });
  it('returns null correctly', function () {
    expect((0, _time.timeFilter)({
      date: null
    }, 'llll')).toBe('Invalid date');
  });
});
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.truncate = void 0;

var _vue = _interopRequireDefault(require("vue"));
/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */


var truncate = function truncate(value, end) {
  if (value.length <= end) {
    return value;
  }

  return value.substring(0, end) + '...';
};

exports.truncate = truncate;

_vue.default.filter('truncate', truncate);

"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.truncateEmail = void 0;

require("core-js/modules/es6.regexp.split");

var _vue = _interopRequireDefault(require("vue"));
/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */


var truncateEmail = function truncateEmail(value, length) {
  if (value.length > length) {
    var truncArray = value.split('@');
    var email = truncArray[0].substring(0, length - truncArray[1].length);
    return email + '...' + '@' + truncArray[1];
  }

  return value;
};

exports.truncateEmail = truncateEmail;

_vue.default.filter('truncateEmail', truncateEmail);
"use strict";

var _interopRequireWildcard = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireWildcard");

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

var _vue = _interopRequireDefault(require("vue"));

var _App = _interopRequireDefault(require("./App.vue"));

var _router = _interopRequireDefault(require("./router"));

var _index = _interopRequireDefault(require("./store/index"));

require("./registerServiceWorker");

var _fontawesomeSvgCore = require("@fortawesome/fontawesome-svg-core");

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _vueFontawesome = require("@fortawesome/vue-fontawesome");

var _veeValidate = require("vee-validate");

var rules = _interopRequireWildcard(require("vee-validate/dist/rules"));

require("./utils/filters/index");

for (var rule in rules) {
  (0, _veeValidate.extend)(rule, rules[rule]);
}

_vue.default.component('ValidationProvider', _veeValidate.ValidationProvider);

_fontawesomeSvgCore.library.add(_freeSolidSvgIcons.faUserSecret, _freeSolidSvgIcons.faArrowRight, _freeSolidSvgIcons.faEnvelope, _freeSolidSvgIcons.faAddressBook, _freeSolidSvgIcons.faPhone, _freeSolidSvgIcons.faKey);

_vue.default.component('font-awesome-icon', _vueFontawesome.FontAwesomeIcon);

_vue.default.config.productionTip = false;
new _vue.default({
  router: _router.default,
  store: _index.default,
  render: function render(h) {
    return h(_App.default);
  }
}).$mount('#app');
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _default = [{
  name: 'primary',
  iconName: ''
}, {
  name: 'secondary',
  iconName: ''
}, {
  name: 'outline',
  iconName: ''
}, {
  name: 'outline-arrow',
  iconName: _freeSolidSvgIcons.faArrowRight
}, {
  name: 'outline-pill',
  iconName: ''
}, {
  name: 'outline-pill-arrow',
  iconName: _freeSolidSvgIcons.faArrowRight
}, {
  name: 'outline-pill-reversed',
  iconName: ''
}, {
  name: 'outline-pill-reversed-arrow',
  iconName: _freeSolidSvgIcons.faArrowRight
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  name: 'primary',
  color: '#B59A76'
}, {
  name: 'secondary',
  color: '#62727B'
}, {
  name: 'success',
  color: '#439889'
}, {
  name: 'danger',
  color: '#FF606A'
}, {
  name: 'warning',
  color: '#F9D982'
}, {
  name: 'info',
  color: '#00838F'
}, {
  name: 'light',
  color: '#8C8C8C'
}, {
  name: 'dark',
  color: '#37474F'
}, {
  name: 'lighter',
  color: '#D9D9D9'
}, {
  name: 'lightest',
  color: '#F8F8F8'
}, {
  name: 'white',
  color: '#FFFFFF'
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _freeSolidSvgIcons = require("@fortawesome/free-solid-svg-icons");

var _default = [{
  name: 'email',
  iconName: _freeSolidSvgIcons.faEnvelope
}, {
  name: 'password',
  iconName: _freeSolidSvgIcons.faKey
}, {
  name: 'phone',
  iconName: _freeSolidSvgIcons.faPhone
}];
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = [{
  img: '',
  title: 'The Westerly',
  description: 'This is the westerly',
  location: 'Nowhere',
  price: 0,
  capacity: 5,
  fav: false
}, {
  img: '',
  title: 'The Grand',
  description: 'This is the Grand',
  location: 'Everywhere',
  price: 0,
  capacity: 10,
  fav: false
}, {
  img: '',
  title: 'The Lincoln',
  description: 'The Lincoln, oh yes.',
  location: 'There',
  price: 0,
  capacity: 300,
  fav: false
}, {
  img: '',
  title: 'The Best',
  description: 'The best of the best',
  location: 'Here',
  price: 0,
  capacity: 20,
  fav: false
}, {
  img: '',
  title: 'The Westerly',
  description: 'This is the westerly',
  location: 'Nowhere',
  price: 0,
  capacity: 5,
  fav: false
}, {
  img: '',
  title: 'The Grand',
  description: 'This is the Grand',
  location: 'Everywhere',
  price: 0,
  capacity: 10,
  fav: false
}];
exports.default = _default;
"use strict";

var _registerServiceWorker = require("register-service-worker");

/* eslint-disable no-console */
if (process.env.NODE_ENV === "production") {
  (0, _registerServiceWorker.register)("".concat(process.env.BASE_URL, "service-worker.js"), {
    ready: function ready() {
      console.log("App is being served from cache by a service worker.\n" + "For more details, visit https://goo.gl/AFskqB");
    },
    registered: function registered() {
      console.log("Service worker has been registered.");
    },
    cached: function cached() {
      console.log("Content has been cached for offline use.");
    },
    updatefound: function updatefound() {
      console.log("New content is downloading.");
    },
    updated: function updated() {
      console.log("New content is available; please refresh.");
    },
    offline: function offline() {
      console.log("No internet connection found. App is running in offline mode.");
    },
    error: function error(_error) {
      console.error("Error during service worker registration:", _error);
    }
  });
}
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _interopRequireWildcard2 = _interopRequireDefault(require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/esm/interopRequireWildcard"));

var _vue = _interopRequireDefault(require("vue"));

var _vueRouter = _interopRequireDefault(require("vue-router"));

// import Home from "./views/Home.vue";
_vue.default.use(_vueRouter.default);

var _default = new _vueRouter.default({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
    path: '/buttons',
    name: 'buttons',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Buttons.vue"));
      });
    }
  }, {
    path: '/colors',
    name: 'colors',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Colors.vue"));
      });
    }
  }, {
    path: '/layout',
    name: 'layout',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Layout.vue"));
      });
    }
  }, {
    path: '*',
    redirect: '/buttons',
    component: function component() {
      return Promise.resolve().then(function () {
        return (0, _interopRequireWildcard2.default)(require("./views/Buttons.vue"));
      });
    }
  }]
});

exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {};
exports.default = _default;
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {};
exports.default = _default;
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _vuex = _interopRequireDefault(require("vuex"));

var _state = _interopRequireDefault(require("./state"));

var _actions = _interopRequireDefault(require("./actions"));

var _getters = _interopRequireDefault(require("./getters"));

var _modules = _interopRequireDefault(require("./modules"));

var _mutations = _interopRequireDefault(require("./mutations"));

_vue.default.use(_vuex.default);

var _default = new _vuex.default.Store({
  state: _state.default,
  getters: _getters.default,
  mutations: _mutations.default,
  actions: _actions.default,
  modules: _modules.default
});

exports.default = _default;
"use strict";
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
var _default = {
  SET_BG_COLOR: function SET_BG_COLOR(state, color) {
    state.bgColor = color;
  },
  SET_EMAIL_FIELD: function SET_EMAIL_FIELD(state, email) {
    state.email = email;
  },
  SET_NAME_FIELD: function SET_NAME_FIELD(state, name) {
    state.name = name;
  },
  SET_PASSWORD_FIELD: function SET_PASSWORD_FIELD(state, password) {
    state.password = password;
  },
  SET_PHONE_FIELD: function SET_PHONE_FIELD(state, phone) {
    state.phone = phone;
  },
  SET_SEARCH: function SET_SEARCH(state, query) {
    state.search = query;
  },
  SET_SEARCH_RESULT: function SET_SEARCH_RESULT(state, result) {
    state.result = result;
  }
};
exports.default = _default;
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _toConsumableArray2 = _interopRequireDefault(require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/esm/toConsumableArray"));

var _venues = _interopRequireDefault(require("../objectRefs/venues"));

var _default = {
  bgColor: '#fff',
  email: '',
  list: ['Apple', 'Banana', 'Orange', 'Mango', 'Pear', 'Peach', 'Grape', 'Tangerine', 'Pineapple', 'San Diego', 'Lake Tahoe', 'Las Vegas', 'New York'],
  name: '',
  password: '',
  phone: '',
  search: '',
  result: '',
  venues: (0, _toConsumableArray2.default)(_venues.default)
};
exports.default = _default;
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.addComma = void 0;

var _vue = _interopRequireDefault(require("vue"));

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */
var addComma = function addComma(value) {
  return value + ', ';
};

exports.addComma = addComma;

_vue.default.filter('addComma', addComma);
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.capitalize = void 0;

var _vue = _interopRequireDefault(require("vue"));

var capitalize = function capitalize(s) {
  return s.charAt(0).toUpperCase() + s.slice(1);
};

exports.capitalize = capitalize;

_vue.default.filter('capitalize', capitalize);
"use strict";

require("./addComma.filter");

require("./capitalize.filter");

require("./lastFour.filter");

require("./lowercase.filter");

require("./percentage.filter");

require("./truncate.filter");

require("./truncateEmail.filter");
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lastFour = void 0;

var _vue = _interopRequireDefault(require("vue"));

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */
var lastFour = function lastFour(value) {
  return value.slice(value.length - 4);
};

exports.lastFour = lastFour;

_vue.default.filter('lastFour', lastFour);
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.lowercase = void 0;

var _vue = _interopRequireDefault(require("vue"));

/**
 *  Filter that accepts a string and makes lowercase all capital letters
 *  @return lowercase
 */
var lowercase = function lowercase(value) {
  return value.toLowerCase();
};

exports.lowercase = lowercase;

_vue.default.filter('lowercase', lowercase);
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.numeralFilter = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _numeral = _interopRequireDefault(require("numeral"));

/**
 *  Filter that accepts a number and format pattern
 *
 *  common patterns:
 *      currency: $0,0.00'
 *      thousands: '0,0'
 *      percent: '0%'
 *
 *  See http://numeraljs.com/ for a complete list of patterns
 *
 *  @return number
 */
var numeralFilter = function numeralFilter(value, pattern) {
  return value && (0, _numeral.default)(value).format(pattern);
};

exports.numeralFilter = numeralFilter;

_vue.default.filter('numeral', numeralFilter);
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.percentageFilter = void 0;

var _vue = _interopRequireDefault(require("vue"));

var percentageFilter = function percentageFilter() {
  var value = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 0;
  var decimals = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
  return "".concat((value * 100).toFixed(decimals), "%");
};

exports.percentageFilter = percentageFilter;

_vue.default.filter('percentage', percentageFilter);
"use strict";

var _percentage = require("./percentage.filter");

describe('percentage.filter', function () {
  var number = 0.842922;
  it('formats number', function () {
    expect((0, _percentage.percentageFilter)(number)).toBe('84%');
  });
  it('formats number when given decimal places', function () {
    expect((0, _percentage.percentageFilter)(number, 2)).toBe('84.29%');
  });
  it('formats null correctly', function () {
    expect((0, _percentage.percentageFilter)(null)).toBe('0%');
  });
  it('formats null correctly when given decimal places', function () {
    expect((0, _percentage.percentageFilter)(null, 2)).toBe('0.00%');
  });
});
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.timeFilter = void 0;

var _vue = _interopRequireDefault(require("vue"));

var _momentTimezone = _interopRequireDefault(require("moment-timezone"));

/**
 *  Filter that accepts an ISO date and format pattern
 *
 *  See https://momentjs.com/timezone/ for patterns
 *
 *  @return string
 */
var timeFilter = function timeFilter(value, format) {
  if (!value || value.hasOwnProperty('date') && !value.date) {
    return 'Invalid date';
  }

  return (0, _momentTimezone.default)(value.date || value).format(format);
};

exports.timeFilter = timeFilter;

_vue.default.filter('time', timeFilter);
"use strict";

var _time = require("./time.filter");

describe('time.filter', function () {
  var unformattedDate = '2018-07-18 18:22:59';
  var formattedDate = 'Wed, Jul 18, 2018 6:22 PM';
  it('returns formatted string', function () {
    expect((0, _time.timeFilter)(unformattedDate, 'llll')).toBe(formattedDate);
  });
  it('returns formatted string when passed an object (value.date)', function () {
    var created = {
      date: unformattedDate
    };
    expect((0, _time.timeFilter)(created.date, 'llll')).toBe(formattedDate);
  });
  it('returns null correctly', function () {
    expect((0, _time.timeFilter)({
      date: null
    }, 'llll')).toBe('Invalid date');
  });
});
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.truncate = void 0;

var _vue = _interopRequireDefault(require("vue"));

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */
var truncate = function truncate(value, end) {
  if (value.length <= end) {
    return value;
  }

  return value.substring(0, end) + '...';
};

exports.truncate = truncate;

_vue.default.filter('truncate', truncate);
"use strict";

var _interopRequireDefault = require("/Users/keith/code/tvr/styleguide/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.truncateEmail = void 0;

require("core-js/modules/es6.regexp.split");

var _vue = _interopRequireDefault(require("vue"));

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */
var truncateEmail = function truncateEmail(value, length) {
  if (value.length > length) {
    var truncArray = value.split('@');
    var email = truncArray[0].substring(0, length - truncArray[1].length);
    return email + '...' + '@' + truncArray[1];
  }

  return value;
};

exports.truncateEmail = truncateEmail;

_vue.default.filter('truncateEmail', truncateEmail);
