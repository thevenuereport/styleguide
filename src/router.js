import Vue from 'vue';
import Router from 'vue-router';
// import Home from "./views/Home.vue";

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/buttons',
      name: 'buttons',
      component: () =>
        import(/* webpackChunkName: "buttons" */ './views/Buttons.vue')
    },
    {
      path: '/colors',
      name: 'colors',
      component: () =>
        import(/* webpackChunkName: "colors" */ './views/Colors.vue')
    },
    {
      path: '/layout',
      name: 'layout',
      component: () =>
        import(/* webpackChunkName: "colors" */ './views/Layout.vue')
    },
    {
      path: '*',
      redirect: '/buttons',
      component: () =>
        import(/* webpackChunkName: "buttons" */ './views/Buttons.vue')
    }
  ]
});
