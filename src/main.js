import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store/index';
import './registerServiceWorker';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faUserSecret,
  faArrowRight,
  faEnvelope,
  faAddressBook,
  faPhone,
  faKey
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { ValidationProvider, extend } from 'vee-validate';
import * as rules from 'vee-validate/dist/rules';
import './utils/filters/index';

for (let rule in rules) {
  extend(rule, rules[rule]);
}

Vue.component('ValidationProvider', ValidationProvider);

library.add(
  faUserSecret,
  faArrowRight,
  faEnvelope,
  faAddressBook,
  faPhone,
  faKey
);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount('#app');
