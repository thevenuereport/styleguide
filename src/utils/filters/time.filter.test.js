import { timeFilter } from './time.filter'

describe('time.filter', () => {
    const unformattedDate = '2018-07-18 18:22:59';
    const formattedDate = 'Wed, Jul 18, 2018 6:22 PM';
    it('returns formatted string', () => {
        expect(timeFilter(unformattedDate, 'llll')).toBe(formattedDate)
    });

    it('returns formatted string when passed an object (value.date)', () => {
        const created = {date: unformattedDate};
        expect(timeFilter(created.date, 'llll')).toBe(formattedDate)
    });

    it('returns null correctly', () => {
        expect(timeFilter({date: null}, 'llll')).toBe('Invalid date')
    });
});
