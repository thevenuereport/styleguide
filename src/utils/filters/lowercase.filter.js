import Vue from 'vue';

/**
 *  Filter that accepts a string and makes lowercase all capital letters
 *  @return lowercase
 */
export const lowercase = value => value.toLowerCase();

Vue.filter('lowercase', lowercase);