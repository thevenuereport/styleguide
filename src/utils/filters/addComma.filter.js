import Vue from 'vue';

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */

export const addComma = function(value) {
    return value + ', ';
};

Vue.filter('addComma', addComma);
