import Vue from 'vue';

export const percentageFilter = (value = 0, decimals = 0) => `${(value * 100).toFixed(decimals)}%`;

Vue.filter('percentage', percentageFilter);
