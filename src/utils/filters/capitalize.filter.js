import Vue from 'vue';

export const capitalize = s => s.charAt(0).toUpperCase() + s.slice(1);

Vue.filter('capitalize', capitalize);
