// Filters
import './addComma.filter';
import './capitalize.filter';
import './lastFour.filter';
import './lowercase.filter';
// import './numeral.filter';
import './percentage.filter';
// import './time.filter';
import './truncate.filter';
import './truncateEmail.filter';
