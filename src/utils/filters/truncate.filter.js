import Vue from 'vue';

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */

export const truncate = function(value, end) {
    if (value.length <= end) {
        return value;
    }
    return value.substring(0, end) + '...';
};

Vue.filter('truncate', truncate);
