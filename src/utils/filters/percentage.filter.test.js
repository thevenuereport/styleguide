import { percentageFilter } from './percentage.filter';

describe('percentage.filter', () => {
  const number = 0.842922;

  it('formats number', () => {
    expect(percentageFilter(number)).toBe('84%');
  });

  it('formats number when given decimal places', () => {
    expect(percentageFilter(number, 2)).toBe('84.29%');
  });

  it('formats null correctly', () => {
    expect(percentageFilter(null)).toBe('0%');
  });

  it('formats null correctly when given decimal places', () => {
    expect(percentageFilter(null, 2)).toBe('0.00%');
  });
});
