import Vue from 'vue';
import moment from 'moment-timezone';

/**
 *  Filter that accepts an ISO date and format pattern
 *
 *  See https://momentjs.com/timezone/ for patterns
 *
 *  @return string
 */
export const timeFilter = (value, format) => {
    if (!value || value.hasOwnProperty('date') && !value.date) {
        return 'Invalid date';
    }

    return moment(value.date || value).format(format);
};

Vue.filter('time', timeFilter);
