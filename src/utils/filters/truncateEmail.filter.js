import Vue from 'vue';

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */

export const truncateEmail = function(value, length) {
    if (value.length > length) {
        let truncArray = value.split('@');
        let email = truncArray[0].substring(0, (length - truncArray[1].length));
        return email + '...' + '@' + truncArray[1];
    }
    return value;
};

Vue.filter('truncateEmail', truncateEmail);
