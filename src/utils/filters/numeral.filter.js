import Vue from 'vue';
import numeral from 'numeral';

/**
 *  Filter that accepts a number and format pattern
 *
 *  common patterns:
 *      currency: $0,0.00'
 *      thousands: '0,0'
 *      percent: '0%'
 *
 *  See http://numeraljs.com/ for a complete list of patterns
 *
 *  @return number
 */
export const numeralFilter = (value, pattern) => value && numeral(value).format(pattern);

Vue.filter('numeral', numeralFilter);
