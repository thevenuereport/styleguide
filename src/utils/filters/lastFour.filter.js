import Vue from 'vue';

/**
 *  Filter that accepts a string and length to be truncated to
 *  and adds elipsis to the end of string
 *  @return string
 */

export const lastFour = value => {
    return value.slice(value.length - 4);
};

Vue.filter('lastFour', lastFour);
